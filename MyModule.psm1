﻿$MyVariable = "Hello, World!"

function Hello {
    param (
        [string]$Name
    )
    Write-Output "Hello, $Name!"
}

function Square {
    param (
        [int]$Number
    )
    return $Number * $Number
}
